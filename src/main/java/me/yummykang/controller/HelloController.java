package me.yummykang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HelloController {
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello big world!");
		return "hello";
	}

	@RequestMapping("/hello")
	public String hello(ModelMap modelMap) {
		modelMap.put("message", "Hello small tom!");
		return "hello";
	}
}