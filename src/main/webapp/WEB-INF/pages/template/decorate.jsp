<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015/7/20
  Time: 9:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>index</title>
    <!-- Required Stylesheets -->
    <link rel="stylesheet" type="text/css" href="../../../css/reset.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../css/text.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../css/fonts/ptsans/stylesheet.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../css/fluid.css" media="screen"/>

    <link rel="stylesheet" type="text/css" href="../../../css/mws.style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../css/icons/icons.css" media="screen"/>

    <!-- Demo and Plugin Stylesheets -->
    <link rel="stylesheet" type="text/css" href="../../../css/demo.css" media="screen"/>

    <link rel="stylesheet" type="text/css" href="../../../plugins/colorpicker/colorpicker.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/jimgareaselect/css/imgareaselect-default.css"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/fullcalendar/fullcalendar.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/fullcalendar/fullcalendar.print.css" media="print"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/tipsy/tipsy.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/sourcerer/Sourcerer-1.2.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/jgrowl/jquery.jgrowl.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../plugins/spinner/spinner.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../../css/jui/jquery.ui.css" media="screen"/>

    <!-- Theme Stylesheet -->
    <link rel="stylesheet" type="text/css" href="../../../css/mws.theme.css" media="screen"/>

    <!-- JavaScript Plugins -->

    <script type="text/javascript" src="../../../js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../plugins/jimgareaselect/jquery.imgareaselect.min.js"></script>
    <script type="text/javascript" src="../../../plugins/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="../../../plugins/jgrowl/jquery.jgrowl.js"></script>
    <script type="text/javascript" src="../../../plugins/jquery.filestyle.js"></script>
    <script type="text/javascript" src="../../../plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="../../../plugins/jquery.dataTables.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../../plugins/flot/excanvas.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../plugins/flot/jquery.flot.min.js"></script>
    <script type="text/javascript" src="../../plugins/flot/jquery.flot.pie.min.js"></script>
    <script type="text/javascript" src="../../plugins/flot/jquery.flot.stack.min.js"></script>
    <script type="text/javascript" src="../../plugins/flot/jquery.flot.resize.min.js"></script>
    <script type="text/javascript" src="../../plugins/colorpicker/colorpicker.js"></script>
    <script type="text/javascript" src="../../plugins/tipsy/jquery.tipsy.js"></script>
    <script type="text/javascript" src="../../plugins/sourcerer/Sourcerer-1.2.js"></script>
    <script type="text/javascript" src="../../plugins/jquery.placeholder.js"></script>
    <script type="text/javascript" src="../../plugins/jquery.validate.js"></script>
    <script type="text/javascript" src="../../plugins/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="../../plugins/spinner/ui.spinner.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <script type="text/javascript" src="../../js/mws.js"></script>
    <script type="text/javascript" src="../../js/demo.js"></script>
    <script type="text/javascript" src="../../js/themer.js"></script>
</head>
<body>
<!-------------------------------- 主题选择start --------------------------------------->
<div id="mws-themer">
    <!-- 主题选择按钮 -->
    <div id="mws-themer-hide"></div>
    <div id="mws-themer-content">
        <div class="mws-themer-section">
            <label for="mws-theme-presets">Presets</label> <select id="mws-theme-presets"></select>
        </div>
        <div class="mws-themer-separator"></div>
        <div class="mws-themer-section">
            <ul>
                <li><span>Base Color</span>

                    <div id="mws-base-cp" class="mws-cp-trigger"></div>
                </li>
                <li><span>Text Color</span>

                    <div id="mws-text-cp" class="mws-cp-trigger"></div>
                </li>
                <li><span>Text Glow Color</span>

                    <div id="mws-textglow-cp" class="mws-cp-trigger"></div>
                </li>
            </ul>
        </div>
        <div class="mws-themer-separator"></div>
        <div class="mws-themer-section">
            <ul>
                <li><span>Text Glow Opacity</span>

                    <div id="mws-textglow-op"></div>
                </li>
            </ul>
        </div>
        <div class="mws-themer-separator"></div>
        <div class="mws-themer-section">
            <button class="mws-button red small" id="mws-themer-getcss">Get CSS</button>
        </div>
    </div>
    <!-- 颜色选择对话框 -->
    <div id="mws-themer-css-dialog">
        <div class="mws-form">
            <div class="mws-form-row" style="padding:0;">
                <div class="mws-form-item">
                    <textarea cols="auto" rows="auto" readonly="readonly"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<!-------------------------------- 主题选择end --------------------------------------->


<div id="mws-header" class="clearfix">
    <!-- logo -->
    <div id="mws-logo-container">
        <div id="mws-logo-wrap">
            <img src="images/mws-logo.png" alt="mws admin"/>
        </div>
    </div>

    <!-- 提醒 -->
    <div id="mws-user-tools" class="clearfix">
        <div id="mws-user-notif" class="mws-dropdown-menu">
            <a href="#" class="mws-i-24 i-alert-2 mws-dropdown-trigger">Notifications</a>
            <span class="mws-dropdown-notif">35</span>

            <div class="mws-dropdown-box">
                <div class="mws-dropdown-content">
                    <ul class="mws-notifications">
                        <li class="read">
                            <a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                        <li class="read">
                            <a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                        <li class="unread">
                            <a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                        <li class="unread">
                            <a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                    </ul>
                    <div class="mws-dropdown-viewall">
                        <a href="#">View All Notifications</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- 消息 -->
        <div id="mws-user-message" class="mws-dropdown-menu">
            <a href="#" class="mws-i-24 i-message mws-dropdown-trigger">Messages</a>
            <span class="mws-dropdown-notif">35</span>

            <div class="mws-dropdown-box">
                <div class="mws-dropdown-content">
                    <ul class="mws-messages">
                        <li class="read">
                            <a href="#">
                                <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                        <li class="read">
                            <a href="#">
                                <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                        <li class="unread">
                            <a href="#">
                                <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                        <li class="unread">
                            <a href="#">
                                <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                            </a>
                        </li>
                    </ul>
                    <div class="mws-dropdown-viewall">
                        <a href="#">View All Messages</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- 用户信息 -->
        <div id="mws-user-info" class="mws-inset">
            <div id="mws-user-photo">
                <img src="example/profile.jpg" alt="User Photo"/>
            </div>
            <div id="mws-user-functions">
                <div id="mws-username">
                    Hello, John Doe
                </div>
                <ul>
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Change Password</a></li>
                    <li><a href="index-2.html">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- 菜单 -->
<div id="mws-wrapper">
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>
    <div id="mws-sidebar">
        <div id="mws-searchbox" class="mws-inset">
            <form action="http://www.malijuwebshop.com/themes/mws-admin/table.html">
                <input type="text" class="mws-search-input"/>
                <input type="submit" class="mws-search-submit"/>
            </form>
        </div>
        <div id="mws-navigation">
            <ul>
                <li><a href="dashboard.html" class="mws-i-24 i-home">Dashboard</a></li>
                <%--<li><a href="charts.html" class="mws-i-24 i-chart">Charts</a></li>
                <li><a href="calendar.html" class="mws-i-24 i-day-calendar">Calendar</a></li>
                <li><a href="files.html" class="mws-i-24 i-file-cabinet">File Manager</a></li>--%>
                <li class="active"><a href="table.html" class="mws-i-24 i-table-1">Table</a></li>
                <li>
                    <a href="#" class="mws-i-24 i-list">Forms</a>
                    <ul>
                        <li><a href="form_layouts.html">Layouts</a></li>
                        <li><a href="form_elements.html">Elements</a></li>
                    </ul>
                </li>
                <%-- <li><a href="widgets.html" class="mws-i-24 i-cog">Widgets</a></li>
                 <li><a href="typography.html" class="mws-i-24 i-text-styling">Typography</a></li>
                 <li><a href="grids.html" class="mws-i-24 i-blocks-images">Grids &amp; Panels</a></li>
                 <li><a href="gallery.html" class="mws-i-24 i-polaroids">Gallery</a></li>
                 <li><a href="error.html" class="mws-i-24 i-alert-2">Error Page</a></li>
                 <li>
                     <a href="icons.html" class="mws-i-24 i-pacman">
                         Icons <span class="mws-nav-tooltip">2000+</span>
                     </a>
                 </li>--%>
            </ul>
        </div>
    </div>

    <!-- 主体内容 -->
    <div id="mws-container" class="clearfix">
        <div class="container">
            <sitemesh:write property='body'/>
            <%--<div class="mws-panel grid_8">
                <div class="mws-panel-header">
                    <span class="mws-i-24 i-table-1">Data Table with Numbered Pagination</span>
                </div>
                <div class="mws-panel-body">
                    <div class="mws-panel-toolbar top clearfix">
                        <ul>
                            <li><a href="#" class="mws-ic-16 ic-accept">Accept</a></li>
                            <li><a href="#" class="mws-ic-16 ic-cross">Reject</a></li>
                            <li><a href="#" class="mws-ic-16 ic-printer">Print</a></li>
                            <li><a href="#" class="mws-ic-16 ic-arrow-refresh">Renew</a></li>
                            <li><a href="#" class="mws-ic-16 ic-edit">Update</a></li>
                        </ul>
                    </div>

                    <table class="mws-datatable-fn mws-table">
                        <thead>
                        <tr>
                            <th style="background-image: none;padding-left: 16px;"><input type="checkbox"/></th>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th>Engine version</th>
                            <th>CSS grade</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeX">
                            <td><input type="checkbox"/></td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 4.0
                            </td>
                            <td>Win 95+</td>
                            <td class="center">4</td>
                            <td class="center">X</td>
                        </tr>
                        <tr class="gradeX">
                            <td><input type="checkbox"/></td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 4.0
                            </td>
                            <td>XXXX</td>
                            <td class="center">4</td>
                            <td class="center">X</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>--%>
        </div>


    </div>
</div>

</div>
</body>
</html>
